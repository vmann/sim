# Build dependencies
 - Meson
 - Ninja
 - C compiler

# Runtime dependencies
 - SDL2

# Build steps
```
meson setup builddir
ninja -C builddir
```
The `sim` binary is in `builddir/sim`. Optionally install it with
```
ninja -C builddir install
```
